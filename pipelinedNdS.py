#! /usr/bin/python3
# Sharlie van der Heide
# Bioinformatics student - internship at ERIBA UMCG
# This script is a pipeline that uses lastZ and PAML to get the dN and dS values for orthologous genes

import sys
import re
import os
from collections import defaultdict
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
from Bio.Alphabet import SingleLetterAlphabet
from Bio import AlignIO
from Bio.Align import MultipleSeqAlignment

def getOrthoGenes(orthoGenesFile):
	# Dictionary with the orthologous genes
	orthoDict = defaultdict(set)
	# Opening the orthologous genes file
	file = open(orthoGenesFile, "r")
	firstLine = file.readline().strip("\n")
	firstlineValues = firstLine.split("\t")
	# Getting the index of the gene ID of organism 1
	geneID1Index = firstlineValues.index("Gene stable ID")
	count = 0
	# Getting the index of the gene ID of organism 2
	for value in firstlineValues:
		if re.search("\w+ gene stable ID", value):
			geneID2Index = count
		else:
			count += 1
	# For each line in the file, the gene IDs will be added to de dictionary.
	# De dictionary contains a set, so there are not geneIDs de same.
	for line in file:
		lineValues = line.split("\t")
		orthoDict[lineValues[geneID1Index]].add(lineValues[geneID2Index])

	return orthoDict

def getDictInDictTranscripts(geneID, organismGenome):
	dictTranscripts = {}
	# Looking for all the transcripts per gene and adding to the DictInDict
	input_seq_iterator = SeqIO.parse(organismGenome, "fasta")
	for record in input_seq_iterator:
		m = re.search("gene:" + geneID + ".", record.description)
		if m:
			# If it is mitochondrial DNA, then -MT is added to the transcriptID
			s = re.search("chromosome:[a-zA-Z0-9_.]+:MT:", record.description)
			if s:
				dictTranscripts[record.id + "-MT"] = str(record.seq)
			else:
				dictTranscripts[record.id] = str(record.seq)

	input_seq_iterator.close()
	return dictTranscripts

def getTranscript(dictInDict):
	dictTranscriptsLength = {}
	MT = False
	# Sorting the transcripsts by length
	for key in dictInDict:
		values = dictInDict.get(key)
		for key2 in values:
			dictTranscriptsLength[key2] = len(values.get(key2))

	listSortedTranscriptsID = sorted(dictTranscriptsLength, key=dictTranscriptsLength.get, reverse=True)
	# Getting the longest transcript
	transcriptID = listSortedTranscriptsID[0]
	# Getting the sequence and the gene ID of the transcript
	for key in dictInDict:
		values = dictInDict.get(key)
		transcriptSeq = values.get(transcriptID)
		for key2 in values:
			if transcriptID == key2:
				geneTranscriptID = key
	# If the transcript ID ends with -MT, then it means this is a mitochondrial transcript
	if transcriptID.endswith("-MT"):
		MT = True
		transcriptID = transcriptID.replace("-MT", "")

	return (geneTranscriptID, transcriptID, transcriptSeq, MT)

def transcriptToFasta(geneTranscriptID, transcriptID, transcriptSeq):
	# Writing for each transcript a fasta file
	record = SeqRecord(Seq(transcriptSeq, SingleLetterAlphabet()), id=transcriptID, description="gene: " + geneTranscriptID)
	SeqIO.write(record, transcriptID + ".fa", "fasta")

def getAlignment(transcriptID1, transcriptID2):
	# Getting the alignment of the two sequences with LastZ, with a maf file as output
	cwd = os.getcwd() + "/"
	outputFileName = transcriptID1 + "-" + transcriptID2
	os.system("lastz " + cwd + transcriptID1 + ".fa " + cwd + transcriptID2 + ".fa --output='" + outputFileName + ".maf' --format=maf")
	return outputFileName

def makePhylipFormat(outputFileName, MTtranscript1, MTtranscript2):
	# Making a Phylip format of the MAF format, because PAML only reads Phylip
	stopCodons = ["TGA", "TAG", "TAA"]
	stopCodonsMT = ["AGA", "AGG", "TAG", "TAA"]
	input_handle = next(AlignIO.parse(outputFileName + ".maf", "maf"))
	output_handle = open(outputFileName + ".phy", "w")
	AlignIO.write(input_handle, output_handle, "phylip-sequential")
	output_handle.close()
	# Checking if the sequence of the alignment contains gaps
	seqGaps = set()
	listAlignments = []
	alignment = AlignIO.read(open(outputFileName + ".phy"), "phylip-sequential")
	for record in alignment:
		seq = str(record.seq)
		# If the sequence contains a gap ("-"), then the index is being added to a set
		if "-" in seq:
			for i in range(len(seq)):
				if "-" == seq[i]:
					seqGaps.add(i)
	# If MTtranscript is True, then this means it is mitochondrial and special stopcodons needs to be used
	if True in (MTtranscript1, MTtranscript2):
		stopCodons = stopCodonsMT
	for record in alignment:
		seq = str(record.seq)
		# Here the gaps are being removed and the stopcodons
		if seqGaps:
			seq = "".join([char for idx, char in enumerate(seq) if idx not in seqGaps])
		else:
			pass
		# If the sequence can not devided by three, PAML can not read this sequence, so then de last nucleotides needs to be deleted
		if len(seq) % 3 == 1:
			seq = seq[:-1]
		elif len(seq) % 3 == 2:
			seq = seq[:-2]
		else:
			pass
		# This is a list of codons of the sequence
		seqCodons = [seq[i:i+3] for i in range(0, len(seq), 3)]		
		# A new sequence is made by deleting the stopcodons
		transcriptSeq = "".join([x for x in seqCodons if x not in stopCodons])
		# A new seq record object is made for each transcript
		listAlignments.append(SeqRecord(Seq(transcriptSeq, SingleLetterAlphabet()), id = record.id[:-2]))
	# Then each object is added to an alignment to make a phylip file
	align = MultipleSeqAlignment(listAlignments)
	AlignIO.write(align, outputFileName + ".phy", "phylip-sequential")

def writeCTLfile(outputFileName, MTtranscript1, MTtranscript2):
	# Here a CTL file is made for PAML
	# If it is a mitochondrial transcript: icode must be changed to 1
	if True in (MTtranscript1, MTtranscript2):
		icode = str(1)
	else:
		icode = str(0)

	ctlFile = open(outputFileName + ".ctl", "w")
	ctlFile.write("seqfile = " + outputFileName + ".phy\n")
	ctlFile.write("outfile = " + outputFileName + "\n")
	ctlFile.write("noisy = 0\n")
	ctlFile.write("verbose = 0\n")
	ctlFile.write("runmode = -2\n")
	ctlFile.write("seqtype = 1\n")
	ctlFile.write("CodonFreq = 2\n")
	ctlFile.write("clock = 0\n")
	ctlFile.write("aaDist = 0\n")
	ctlFile.write("aaRatefile = 'wag.dat'\n")
	ctlFile.write("model = 0\n")
	ctlFile.write("NSsites = 0\n")
	ctlFile.write("icode = " + icode + "\n")
	ctlFile.write("Mgene = 0\n")
	ctlFile.write("fix_kappa = 0\n")
	ctlFile.write("kappa = 2\n")
	ctlFile.write("fix_omega = 0\n")
	ctlFile.write("omega = .4\n")
	ctlFile.write("fix_alpha = 1\n")
	ctlFile.write("alpha = 0.\n")
	ctlFile.write("Malpha = 0\n")
	ctlFile.write("ncatG = 8\n")
	ctlFile.write("getSE = 0\n")
	ctlFile.write("RateAncestor = 1\n")
	ctlFile.write("Small_Diff = .5e-6\n")
	ctlFile.write("cleandata = 1\n")
	ctlFile.write("method = 0\n")

	ctlFile.close()

def getdNdSValues(outputFileName):
	# Running PAML to get the dN and dS values
	cwd = os.getcwd() + "/"
	os.system("codeml " + cwd + outputFileName + ".ctl")

	file = open(outputFileName, "r")
	
	# Get these values out of the output file
	for line in file:
		dNdSPattern = re.search("dN/dS=  ([0-9.]+)  dN = ([0-9.]+)  dS = ([0-9.]+)", line)
		if dNdSPattern:
			dNdS = dNdSPattern.group(1)
			dN = dNdSPattern.group(2)
			dS = dNdSPattern.group(3)

	return(dNdS, dN, dS)

def deleteTranscriptFiles(transcriptID):
	# Deleting the fasta files
	cwd = os.getcwd() + "/"
	os.remove(cwd + transcriptID + ".fa")

def deleteOutputFiles(outputFileName):
	# Deleting all the output files
	cwd = os.getcwd() + "/"
	os.remove(cwd + outputFileName + ".maf")
	os.remove(cwd + outputFileName + ".ctl")
	os.remove(cwd + outputFileName + ".phy")
	os.remove(cwd + outputFileName)

def main():
	orthoGenesFile = sys.argv[1]
	organismGenome1 = sys.argv[2]
	organismGenome2 = sys.argv[3]

	organism1 = os.path.splitext(os.path.basename(organismGenome1))[0]
	organism2 = os.path.splitext(os.path.basename(organismGenome2))[0]

	outputFile = open(organism1 + "-" + organism2 + "-orthologs.txt", "w")
	outputFile.write("gene ID " + organism1 + "\ttranscript ID " + organism1 + "\tgene ID " + organism2 + "\ttranscript ID " + organism2 + "\tdN\tdS\tdN/dS\n")

	orthoDict = getOrthoGenes(orthoGenesFile)
	for geneID1 in orthoDict:
		geneID2 = orthoDict.get(geneID1)
		dictInDict1 = {}
		dictTranscripts1 = getDictInDictTranscripts(geneID1, organismGenome1)
		dictInDict1[geneID1] = dictTranscripts1
		dictInDict2 = {}
		for geneID in geneID2:
			dictTranscripts2 = getDictInDictTranscripts(geneID, organismGenome2)
			dictInDict2[geneID] = dictTranscripts2
		try:
			transcript1 = getTranscript(dictInDict1)
			geneTranscriptID1 = transcript1[0]
			transcriptID1 = transcript1[1]
			transcriptSeq1 = transcript1[2]
			MTtranscript1 = transcript1[3]
			transcriptToFasta(geneTranscriptID1, transcriptID1, transcriptSeq1)
			for key in dictInDict2:
				newDict = {}
				values = dictInDict2.get(key)
				newDict[key] = values
				transcript2 = getTranscript(newDict)
				geneTranscriptID2 = transcript2[0]
				transcriptID2 = transcript2[1]
				transcriptSeq2 = transcript2[2]
				MTtranscript2 = transcript2[3]
				transcriptToFasta(geneTranscriptID2, transcriptID2, transcriptSeq2)
				outputFileName = getAlignment(transcriptID1, transcriptID2)
				try:
					makePhylipFormat(outputFileName, MTtranscript1, MTtranscript2)
					writeCTLfile(outputFileName, MTtranscript1, MTtranscript2)
					values = getdNdSValues(outputFileName)
					dNdS = values[0]
					dN = values[1]
					dS = values[2]
				except:
					dNdS = "NA"
					dN = "NA"
					dS = "NA"
				outputFile.write(geneTranscriptID1 + "\t" + transcriptID1 + "\t" + geneTranscriptID2 + "\t" + transcriptID2 + "\t" + dN + "\t" + dS + "\t" + dNdS + "\n")
				deleteTranscriptFiles(transcriptID2)
				deleteOutputFiles(outputFileName)
			deleteTranscriptFiles(transcriptID1)
		except:
			pass
		
	outputFile.close()

if __name__ == "__main__":
	main()

